/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pupi
 */
@XmlRootElement
public class RootObject extends AbstractModel{
    private List<Section> sections;

    public RootObject(List<Section> sections) {
        this.sections = sections;
    }

    public RootObject() {
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
    
    
}
