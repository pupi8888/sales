/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.model.Product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zettasoftware.sales.model.AbstractModel;

/**
 *
 * @author pupi
 */
 @JsonIgnoreProperties(ignoreUnknown = true)
public class PriceProduct extends AbstractModel{
    public Integer list_price;
    public Integer price;
    public String list_price_description;
    public String price_description;
    public Integer discount;
    public String discount_description;

    public PriceProduct() {
    }

    public Integer getList_price() {
        return list_price;
    }

    public void setList_price(Integer list_price) {
        this.list_price = list_price;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getList_price_description() {
        return list_price_description;
    }

    public void setList_price_description(String list_price_description) {
        this.list_price_description = list_price_description;
    }

    public String getPrice_description() {
        return price_description;
    }

    public void setPrice_description(String price_description) {
        this.price_description = price_description;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getDiscount_description() {
        return discount_description;
    }

    public void setDiscount_description(String discount_description) {
        this.discount_description = discount_description;
    }
    
    
    
}
