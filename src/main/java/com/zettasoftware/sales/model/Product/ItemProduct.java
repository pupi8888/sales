/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.model.Product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zettasoftware.sales.model.AbstractModel;

/**
 *
 * @author pupi
 */
 @JsonIgnoreProperties(ignoreUnknown = true)
public class ItemProduct extends AbstractModel {
    public String xid;
    public String description;
    public String brand ;
    public String catalog_id ;
    public int category_id;
    public String url;
    public String web_url;
    public PriceProduct price;

    public ItemProduct() {
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCatalog_id() {
        return catalog_id;
    }

    public void setCatalog_id(String catalog_id) {
        this.catalog_id = catalog_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public PriceProduct getPrice() {
        return price;
    }

    public void setPrice(PriceProduct price) {
        this.price = price;
    }
    
    
}
