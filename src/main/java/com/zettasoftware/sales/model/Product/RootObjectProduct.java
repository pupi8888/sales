/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.model.Product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zettasoftware.sales.model.AbstractModel;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author pupi
 */
 @JsonIgnoreProperties(ignoreUnknown = true)
public class RootObjectProduct extends AbstractModel{
    private List<ItemProduct> items;

    public RootObjectProduct() {
    }

    public RootObjectProduct(List<ItemProduct> items) {
        this.items = items;
    }

    public List<ItemProduct> getItems() {
        return items;
    }

    public void setItems(List<ItemProduct> items) {
        this.items = items;
    }
    
    
}
