/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pupi
 */
@XmlRootElement
public class Item extends AbstractModel{
    private String id;
    private String type;
    private String description;
    private String web_url;

    public Item() {
    }

    public Item(String id, String type, String description, String web_url) {
        this.id = id;
        this.type = type;
        this.description = description;
        this.web_url = web_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }
    
    
    
}
