/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zettasoftware.sales.controller;

/**
 *
 * @author pupi
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zettasoftware.sales.model.RootObject;
import com.zettasoftware.sales.model.Section;
import com.zettasoftware.sales.model.Item;
import com.zettasoftware.sales.model.Product.ItemProduct;
import com.zettasoftware.sales.model.Product.RootObjectProduct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@ManagedBean(name = "productos", eager = true)
public class ProductosController {

    List<ItemProduct> productos = new ArrayList<ItemProduct>();
    private String query = "4323"; //HARCODED TO TEST!
    
    public ProductosController() {
        System.out.println("HelloWorld started!");
        
    }

    public List<ItemProduct> getProductos() throws IOException {
        if (query.isEmpty()) {
            return new ArrayList<ItemProduct>();
        }
        
        OkHttpClient client = new OkHttpClient();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://mapi.garbarino.com/products").newBuilder();
        urlBuilder.addQueryParameter("search_type", "category")
                .addQueryParameter("q", this.query);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("x-api-key", "32223507-c36c-42c2-b52d-b3f1d65fa5ae")
                .addHeader("x-brand", "garbarino")
                .addHeader("x-client-info", "eyJhcHBfaW5zdGFsbGF0aW9uX2lkIjoiNzYzMmE5NWUtOGZjNi00NDMyLTkyNWUtNDk3YjYzMzk5MjUwIiwiYXBwX3ZlcnNpb24iOiIyLjE0LjEiLCJicmFuZCI6Ik1vdG9yb2xhIiwiY2hhbm5lbCI6Ik1PQklMRV9BUFAiLCJtb2RlbCI6Ik1vdG9HMyIsIm9zIjoiQU5EUk9JRCIsIm9zX3ZlcnNpb24iOiIyMyJ9")
                .build();
        
        Response response = client.newCall(request).execute();
        String jsonData = response.body().string();  
        ObjectMapper mapper = new ObjectMapper();
        RootObjectProduct resp = mapper.readValue(jsonData, RootObjectProduct.class);
        return resp.getItems();
    }
    
    public String getMessage() {
        return "Hello World!";
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
    
    
    
}
